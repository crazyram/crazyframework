﻿using CrazyRam.Core.Initialization;
using UnityEngine;

namespace CrazyRam.Core.Framework
{
    public abstract class ResourceInitializerBase<T> : MonoBehaviour, IInitializer where T : IRootState<T>
    {
        protected T State;

        public abstract void Init();

        private void Update()
        {
            var systems = State.Systems;
            for (int i = 0; i < systems.Length; i++)
                systems[i].Update(State);
        }

        private void OnApplicationQuit()
        {
            OnDispose();
            ReferenceResolver.Temporal.Flush();
            ReferenceResolver.Instance.Flush();
        }

        protected virtual void OnDispose()
        {
            var systems = State.Systems;
            for (int i = 0; i < systems.Length; i++)
                systems[i].Update(State);
        }
    }
}
