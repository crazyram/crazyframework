namespace CrazyRam.Core.Framework
{
    public interface IRootState<in T> : IState where T : IState
    {
        // todo use C# 9.0 function pointers delegate*<T>[] ptr;
        IGameSystem<T>[] Systems { get; }
    }
}
