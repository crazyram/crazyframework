﻿namespace CrazyRam.Core.Framework
{
    public interface IGameSystem<in T> where T: IState
    {
        void Init(T state);

        void Update(T state);
    }
}
