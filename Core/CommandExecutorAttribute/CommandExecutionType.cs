﻿namespace CrazyRam.Core.Framework
{
    public enum CommandExecutionType
    {
        UI = 0,
        Visual = 1,
        Server = 2
    }
}
