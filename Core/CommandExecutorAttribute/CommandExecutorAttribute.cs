﻿using System;

namespace CrazyRam.Core.Framework
{
    [AttributeUsage(AttributeTargets.Struct)]
    public class CommandExecutorAttribute : Attribute
    {
        public CommandExecutionType CommandExecution { get; }

        public CommandExecutorAttribute(CommandExecutionType commandExecution)
        {
            CommandExecution = commandExecution;
        }
    }
}
