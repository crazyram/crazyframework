﻿using System;
using System.Runtime.CompilerServices;
using System.Threading;

namespace CrazyRam.Core.Framework
{
    //Just an int wrapper
    public readonly struct Id<T> : IEquatable<Id<T>>
    {
        public readonly int Value;

        // ReSharper disable once StaticMemberInGenericType
        private static int _next;

        public Id(int id)
        {
            Value = id;
        }

        public bool IsValid
        {
            [MethodImpl(MethodImplOptions.AggressiveInlining)]
            get => Value >= 0;
        }

        public static Id<T> Invalid => new Id<T>(-1);

        public static Id<T> Next()
        {
            var result = new Id<T>(_next);
            Interlocked.Increment(ref _next);
            return result;
        }

        public static Id<T> NextEven()
        {
            //check even
            Id<T> result;
            // todo use branch free version
            if ((_next & 1) == 0)
            {
                result = new Id<T>(_next);
                Interlocked.Increment(ref _next);
            }
            else
            {
                result = new Id<T>(_next + 1);
                Interlocked.Add(ref _next, 2);
            }

            return result;
        }

        public static Id<T> NextOdd()
        {
            //check odd
            Id<T> result;
            if ((_next & 1) == 0)
            {
                result = new Id<T>(_next + 1);
                Interlocked.Add(ref _next, 2);
            }
            else
            {
                result = new Id<T>(_next);
                Interlocked.Increment(ref _next);
            }

            return result;
        }

        public static void Reset()
        {
            _next = 0;
        }

        public override bool Equals(object obj)
        {
            if (ReferenceEquals(null, obj)) return false;
            return obj is Id<T> id && Equals(id);
        }

        public bool Equals(Id<T> other)
        {
            return Value == other.Value;
        }

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public static bool operator ==(Id<T> a, Id<T> b)
        {
            return a.Equals(b);
        }

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public static bool operator !=(Id<T> a, Id<T> b)
        {
            return !(a == b);
        }

        public override int GetHashCode()
        {
            return Value;
        }

        public override string ToString()
        {
            return $"Id of {typeof(T)} {Value}";
        }
    }
}
