﻿using System.Runtime.CompilerServices;

namespace CrazyRam.Core.Framework
{
    public static class IdManager
    {
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public static Id<T> Next<T>()
        {
            return Id<T>.Next();
        }

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public static Id<T> NextEven<T>()
        {
            return Id<T>.NextEven();
        }

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public static Id<T> NextOdd<T>()
        {
            return Id<T>.NextOdd();
        }

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public static void Reset<T>()
        {
            Id<T>.Reset();
        }

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public static Id<T> ToId<T>(this int value)
        {
            return new Id<T>(value);
        }
    }
}
