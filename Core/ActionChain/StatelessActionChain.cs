﻿using System.Runtime.CompilerServices;
using CrazyRam.Core.Collections;

namespace CrazyRam.Core.Framework
{
    public class StatelessActionChain : ITickable
    {
        private int _currentTask;

        private readonly CrazyList<ITickable> _tickables;

        public bool IsTerminated { get; private set; }

        public StatelessActionChain()
        {
            _tickables = new CrazyList<ITickable>();
            _currentTask = 0;
        }

        public StatelessActionChain Push(ITickable tickable)
        {
            _tickables.Add(tickable);
            return this;
        }

        private void Init()
        {
            _tickables.Clear();
            IsTerminated = false;
        }

        public void Start()
        {
            if (_tickables.Count == 0)
            {
                IsTerminated = true;
                return;
            }

            _currentTask = 0;
            _tickables[_currentTask].Start();
            HoldTermination(_tickables[_currentTask]);
        }

        public void Tick()
        {
            var effector = _tickables[_currentTask];
            effector.Tick();
            HoldTermination(effector);
        }

        public void Terminate()
        {
            if (_tickables.InBounds(_currentTask))
            {
                for (int i = _currentTask; i < _tickables.Count; i++)
                    _tickables[i].SafeTerminate();
            }

            _tickables.Clear();
            PoolFactory.Push(this);
        }

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        private void HoldTermination(ITickable effector)
        {
            while (effector.IsTerminated)
            {
                effector.Terminate();
                _currentTask++;
                if (_currentTask >= _tickables.Count)
                {
                    IsTerminated = true;
                    return;
                }

                effector = _tickables[_currentTask];
                effector.Start();
            }
        }

        public static StatelessActionChain Next()
        {
            var chain = PoolFactory.NextOrNew<StatelessActionChain>();
            chain.Init();
            return chain;
        }
    }
}
