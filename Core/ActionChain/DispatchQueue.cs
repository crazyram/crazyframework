﻿public enum DispatchQueue : byte
{
    Update = 0,
    PhysicsUpdate = 1
}
