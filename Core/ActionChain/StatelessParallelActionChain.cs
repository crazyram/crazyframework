﻿using CrazyRam.Core.Collections;

namespace CrazyRam.Core.Framework
{
    public class StatelessParallelActionChain : ITickable
    {
        private readonly CrazyList<ITickable> _tickables;

        public bool IsTerminated { get; private set; }

        public StatelessParallelActionChain()
        {
            _tickables = new CrazyList<ITickable>();
        }

        public StatelessParallelActionChain Push(ITickable tickable)
        {
            _tickables.Add(tickable);
            return this;
        }

        private void Init()
        {
            IsTerminated = false;
        }

        public void Start()
        {
            for (int i = 0; i < _tickables.Count; i++)
                _tickables[i].Start();
        }

        public void Tick()
        {
            if (_tickables == null)
                return;

            bool allTerminated = true;
            for (int i = 0; i < _tickables.Count; i++)
            {
                var tween = _tickables[i];
                if (tween == null || tween.IsTerminated)
                    continue;
                allTerminated = false;
                tween.Tick();
                if (!tween.IsTerminated)
                    continue;
                tween.Terminate();
                _tickables[i] = null;
            }

            IsTerminated = allTerminated;
        }

        public void Terminate()
        {
            for (int i = 0; i < _tickables.Count; i++)
                _tickables[i].SafeTerminate();

            _tickables.Clear();
            PoolFactory.Push(this);
        }

        public static StatelessParallelActionChain Next()
        {
            var next = PoolFactory.NextOrNew<StatelessParallelActionChain>();
            next.Init();
            return next;
        }
    }
}
