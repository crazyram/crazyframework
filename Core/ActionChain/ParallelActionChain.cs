﻿using CrazyRam.Core.Collections;

namespace CrazyRam.Core.Framework
{
    public class ParallelActionChain<T> : ITickable
    {
        private readonly CrazyList<ITickable<T>> _tickables;

        public bool IsTerminated { get; private set; }

        private T _data;

        public ParallelActionChain()
        {
            _tickables = new CrazyList<ITickable<T>>();
        }

        public ParallelActionChain<T> Push(ITickable<T> tickable)
        {
            _tickables.Add(tickable);
            return this;
        }

        private void Init(T data)
        {
            _data = data;
            _tickables.Clear();
            IsTerminated = false;
        }

        public void Start()
        {
            for (int i = 0; i < _tickables.Count; i++)
                _tickables[i].Start(_data);
        }

        public void Tick()
        {
            if (_tickables == null)
                return;

            bool allTerminated = true;
            for (int i = 0; i < _tickables.Count; i++)
            {
                var tween = _tickables[i];
                if (tween == null || tween.IsTerminated)
                    continue;
                allTerminated = false;
                tween.Tick(_data);
                if (!tween.IsTerminated)
                    continue;
                tween.Terminate(_data);
                _tickables[i] = null;
            }

            IsTerminated = allTerminated;
        }

        public void Terminate()
        {
            for (int i = 0; i < _tickables.Count; i++)
                _tickables[i].SafeTerminate(_data);

            _tickables.Clear();
            PoolFactory.Push(this);
        }

        public static ParallelActionChain<T> Next(T data)
        {
            var next = PoolFactory.NextOrNew<ParallelActionChain<T>>();
            next.Init(data);
            return next;
        }
    }
}
