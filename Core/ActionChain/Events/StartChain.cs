﻿using CrazyRam.Core.Collections;

namespace CrazyRam.Core.Framework
{
    public struct StartChain
    {
        public readonly ITickable Chain;

        public SlotId Slot;

        public StartChain(ITickable chainBase)
        {
            Chain = chainBase;
            Slot = default;
        }

        public StartChain(ITickable chain, SlotId slot)
        {
            Chain = chain;
            Slot = slot;
        }
    }
}
