﻿using CrazyRam.Core.Collections;

namespace CrazyRam.Core.Framework
{
    public readonly struct StopChain
    {
        public readonly SlotId ChainId;

        public StopChain(SlotId chainId)
        {
            ChainId = chainId;
        }
    }
}
