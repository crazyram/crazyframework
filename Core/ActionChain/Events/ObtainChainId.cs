﻿using CrazyRam.Core.Collections;

namespace CrazyRam.Core.Framework
{
    public struct ObtainChainId
    {
        public SlotId ResponseId;
    }
}
