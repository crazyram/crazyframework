﻿namespace CrazyRam.Core.Framework
{
    public abstract class ChainTickableBase<TChainData, TParams> : ITickable<TChainData>
    {
        protected TParams Params;

        public bool IsTerminated { get; protected set; }

        private ITickable<TChainData> Init(TParams data)
        {
            Params = data;
            IsTerminated = false;
            return this;
        }

        public abstract void Start(TChainData data);

        public abstract void Tick(TChainData data);

        public abstract void Terminate(TChainData data);

        protected static ITickable<TChainData> Next<TV>(TParams data)
            where TV : ChainTickableBase<TChainData, TParams>, new()
        {
            return PoolFactory
                .NextOrNew<TV>()
                .Init(data);
        }
    }
}
