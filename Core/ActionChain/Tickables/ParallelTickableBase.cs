﻿using CrazyRam.Core.Collections;

namespace CrazyRam.Core.Framework
{
    public class ParallelTickableBase<T> : ITickable<T>
    {
        private readonly CrazyList<ITickable<T>> _tickables;

        public bool IsTerminated { get; private set; }

        public ParallelTickableBase()
        {
            _tickables = new CrazyList<ITickable<T>>();
        }

        public ParallelTickableBase<T> Push(ITickable<T> tickable)
        {
            _tickables.Add(tickable);
            return this;
        }

        private void Init(T data)
        {
            _tickables.Clear();
            IsTerminated = false;
        }

        public void Start(T data)
        {
            for (int i = 0; i < _tickables.Count; i++)
                _tickables[i].Start(data);
        }

        public void Tick(T data)
        {
            if (_tickables == null)
                return;

            bool allTerminated = true;
            for (int i = 0; i < _tickables.Count; i++)
            {
                var tween = _tickables[i];
                if (tween == null || tween.IsTerminated)
                    continue;
                allTerminated = false;
                tween.Tick(data);
                if (!tween.IsTerminated)
                    continue;
                tween.Terminate(data);
                _tickables[i] = null;
            }

            IsTerminated = allTerminated;
        }

        public void Terminate(T data)
        {
            for (int i = 0; i < _tickables.Count; i++)
                _tickables[i].SafeTerminate(data);

            _tickables.Clear();
            PoolFactory.Push(this);
        }

        public static ParallelTickableBase<T> Next(T data)
        {
            var next = PoolFactory.NextOrNew<ParallelTickableBase<T>>();
            next.Init(data);
            return next;
        }
    }
}
