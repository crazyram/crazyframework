﻿namespace CrazyRam.Core.Framework
{
    public abstract class ChainLaunchTickable<T> : ChainTickableBase<T, ChainLaunchDescription>
    {
        public override void Tick(T data)
        {
        }

        public override void Terminate(T data)
        {
            if (!Params.Launched)
                Params.Chain?.SafeTerminate();
        }
    }
}
