﻿namespace CrazyRam.Core.Framework
{
    public static class TickableHelper
    {
        public static void SafeTerminate<T>(this ITickable<T> tickable, T data)
        {
            if (tickable is { IsTerminated: false })
                tickable.Terminate(data);
        }

        public static void SafeTerminate(this ITickable tickable)
        {
            if (tickable is { IsTerminated: false })
                tickable.Terminate();
        }
    }
}
