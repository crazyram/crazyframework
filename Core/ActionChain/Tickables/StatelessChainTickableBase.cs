﻿namespace CrazyRam.Core.Framework
{
    public abstract class StatelessChainTickableBase<TParams> : ITickable where TParams : struct
    {
        protected TParams Params;

        public bool IsTerminated { get; protected set; }

        private ITickable Init(TParams @params)
        {
            IsTerminated = false;
            Params = @params;
            return this;
        }

        public abstract void Start();

        public abstract void Tick();

        public abstract void Terminate();

        protected static ITickable Next<TV>(TParams data) where TV : StatelessChainTickableBase<TParams>, new()
        {
            return PoolFactory
                .NextOrNew<TV>()
                .Init(data);
        }
    }
}
