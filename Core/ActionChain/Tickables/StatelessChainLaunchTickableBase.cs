﻿namespace CrazyRam.Core.Framework
{
    public struct ChainLaunchDescription
    {
        public ITickable Chain;

        public DispatchQueue Queue;

        public bool Launched;

        public ChainLaunchDescription(ITickable chain)
        {
            Chain = chain;
            Queue = 0;
            Launched = false;
        }

        public ChainLaunchDescription(ITickable chain, DispatchQueue queue)
        {
            Chain = chain;
            Queue = queue;
            Launched = false;
        }
    }

    public abstract class StatelessChainLaunchTickableBase : StatelessChainTickableBase<ChainLaunchDescription>
    {
        public override void Tick()
        {
        }

        public override void Terminate()
        {
            if (!Params.Launched)
                Params.Chain?.Terminate();
        }
    }
}
