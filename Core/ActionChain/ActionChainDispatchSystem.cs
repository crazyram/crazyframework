﻿using System.Runtime.CompilerServices;
using CrazyRam.Core.Collections;
using CrazyRam.Core.MessageBus;

namespace CrazyRam.Core.Framework
{
    public abstract class ActionChainDispatchSystem<T> : IGameSystem<T>, IMessageReceiver<StopChain>,
        IPointerMessageReceiver<StartChain>, IMessageReceiver<StartChain>, IPointerMessageReceiver<ObtainChainId>
        where T: IState
    {
        protected readonly DenseSlotMap<ITickable> Chains;

        protected ActionChainDispatchSystem()
        {
            Chains = new DenseSlotMap<ITickable>();
        }

        public virtual void Init(T state)
        {
        }

        public abstract void Update(T state);

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        protected static void DispatchUpdate(DenseSlotMap<ITickable> chains)
        {
            for (int i = 0; i < chains.Count; i++)
            {
                var tickable = chains.Values[i];
                if (!tickable.IsTerminated)
                    tickable.Tick();
                else
                {
                    tickable.Terminate();
                    chains.RemoveAt(i);
                    i--;
                }
            }
        }

        protected static SlotId ObtainId(DenseSlotMap<ITickable> chains)
        {
            return chains.Push(null);
        }

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        protected static SlotId PushChain(DenseSlotMap<ITickable> chains, ITickable tickable)
        {
            if (chains.Contains(tickable))
                UnityEngine.Debug.Log("Duplicate chain addition");
            var result = chains.Push(tickable);
            tickable.Start();
            return result;
        }

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        protected static void PushChainWithSlot(DenseSlotMap<ITickable> chains, SlotId slot, ITickable tickable)
        {
            chains[slot] = new Option<ITickable>(tickable);
            tickable.Start();
        }

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        protected static void RemoveChain(DenseSlotMap<ITickable> chains, SlotId chainId)
        {
            var chain = chains[chainId];
            if (chain.HasValue)
                chain.Value?.Terminate();
            chains.Remove(chainId);
        }

        public void OnRefMessage(ref ObtainChainId obtainRequest)
        {
            obtainRequest.ResponseId = ObtainId(Chains);
        }

        public void OnMessage(in StartChain start)
        {
            PushChainWithSlot(Chains, start.Slot, start.Chain);
        }

        public void OnRefMessage(ref StartChain start)
        {
            var slot = PushChain(Chains, start.Chain);
            start.Slot = slot;
        }

        public void OnMessage(in StopChain stop)
        {
            RemoveChain(Chains, stop.ChainId);
        }
    }
}
