﻿using System.Runtime.CompilerServices;
using CrazyRam.Core.Collections;

namespace CrazyRam.Core.Framework
{
    public class ActionChain<T> : ITickable
    {
        private T _data;

        private int _currentTask;

        private readonly CrazyList<ITickable<T>> _tickables;

        public bool IsTerminated { get; private set; }

        public ActionChain()
        {
            _tickables = new CrazyList<ITickable<T>>();
            _currentTask = 0;
        }

        public ActionChain<T> Push(ITickable<T> tickable)
        {
            _tickables.Add(tickable);
            return this;
        }

        private void Init(T data)
        {
            _data = data;
            _tickables.Clear();
            IsTerminated = false;
        }

        public void Start()
        {
            if (_tickables.Count == 0)
            {
                IsTerminated = true;
                return;
            }

            _currentTask = 0;
            _tickables[_currentTask].Start(_data);
            HoldTermination(_tickables[_currentTask]);
        }

        public void Tick()
        {
            var effector = _tickables[_currentTask];
            effector.Tick(_data);
            HoldTermination(effector);
        }

        public void Terminate()
        {
            if (_tickables.InBounds(_currentTask))
            {
                for (int i = _currentTask; i < _tickables.Count; i++)
                    _tickables[i].SafeTerminate(_data);
            }

            _tickables.Clear();
            PoolFactory.Push(this);
        }

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        private void HoldTermination(ITickable<T> effector)
        {
            while (effector.IsTerminated)
            {
                effector.Terminate(_data);
                _currentTask++;
                if (_currentTask >= _tickables.Count)
                {
                    IsTerminated = true;
                    return;
                }

                effector = _tickables[_currentTask];
                effector.Start(_data);
            }
        }

        public static ActionChain<T> Next(T data)
        {
            var chain = PoolFactory.NextOrNew<ActionChain<T>>();
            chain.Init(data);
            return chain;
        }
    }
}
