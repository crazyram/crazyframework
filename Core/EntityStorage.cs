﻿using System.Collections.Generic;
using System.Runtime.CompilerServices;

namespace CrazyRam.Core.Framework
{
    public class EntityStorage<T> : Dictionary<Id<T>, T>
    {
        public T this[int id]
        {
            [MethodImpl(MethodImplOptions.AggressiveInlining)]
            get => this[new Id<T>(id)];
            [MethodImpl(MethodImplOptions.AggressiveInlining)]
            set => this[new Id<T>(id)] = value;
        }
    }
}
