﻿namespace CrazyRam.Core.Framework
{
    public interface ITickable
    {
        bool IsTerminated { get; }

        void Start();

        void Tick();

        void Terminate();
    }

    public interface ITickable<in T>
    {
        bool IsTerminated { get; }

        void Start(T data);

        void Tick(T data);

        void Terminate(T data);
    }
}
