﻿namespace CrazyRam.Core.Framework
{
    public interface IRefTickable<T>
    {
        bool IsTerminated { get; }

        void Start(ref T data);

        void Tick(ref T data);

        void Terminate(ref T data);
    }
}
