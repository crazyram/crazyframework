﻿using System.Runtime.CompilerServices;

namespace CrazyRam.Core.Framework
{
    public interface ICommand
    {
#if ZERO_FORMATTER_SERIALIZATION
        [ZeroFormatter.IgnoreFormat]
#endif
        byte Key
        {
            [MethodImpl(MethodImplOptions.AggressiveInlining)]
            get;
        }
    }
}
