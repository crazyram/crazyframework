﻿namespace CrazyRam.Core.Framework
{
    public interface ILevelCommand<in T> : ICommand where T : IState
    {
        void Execute(T state);
    }
}
