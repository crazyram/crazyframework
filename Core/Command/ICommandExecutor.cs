﻿namespace CrazyRam.Core.Framework
{
    public interface ICommandExecutor<out TServerState, out TClientState>
        where TServerState : IState
        where TClientState : IState
    {
        //Execute visual command for current user only(local host)
        void ExecuteUI<T>(ref T command) where T : struct, ILevelCommand<TClientState>;

        //Execute visual command(by network)
        void ExecuteVisual<T>(ref T command) where T : struct, ILevelCommand<TClientState>;

        //Execute server command(network/local host)
        void ExecuteServer<T>(ref T command) where T : struct, ILevelCommand<TServerState>;
    }
}
